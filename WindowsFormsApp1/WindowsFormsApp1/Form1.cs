﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Net;
using System.Net.Http;
using System.Xml;
using System.IO;
using RestSharp;
using Newtonsoft.Json;
using System.Xml.Serialization;
using Services;
using Services.BancoBrasil;
using static Services.BancoBrasil.RequestRegistroBoleto;

namespace WindowsFormsApp1
{
    public partial class Form1 : Form
    {
        //resposta _resposta = new resposta();

        public Form1()
        {
            InitializeComponent();
        }
        /// <summary>
        ///  Metodo utilizado para fazer a authenticação e e envio do registro do boleto
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void button1_Click(object sender, EventArgs e)
        {
            // chaves utilizadas para fazer a authenticaçao  no sistema do banco do brasil
            var ClientID = "eyJpZCI6IjgwNDNiNTMtZjQ5Mi00YyIsImNvZGlnb1B1YmxpY2Fkb3IiOjEwOSwiY29kaWdvU29mdHdhcmUiOjEsInNlcXVlbmNpYWxJbnN0YWxhY2FvIjoxfQ";
            var ClientSecret = "eyJpZCI6IjBjZDFlMGQtN2UyNC00MGQyLWI0YSIsImNvZGlnb1B1YmxpY2Fkb3IiOjEwOSwiY29kaWdvU29mdHdhcmUiOjEsInNlcXVlbmNpYWxJbnN0YWxhY2FvIjoxLCJzZXF1ZW5jaWFsQ3JlZGVuY2lhbCI6MX0";

            /// Esté objeto é um exemplo que deve ser preenchido com os dados do boleto
            RequestRegistroBoleto.Envelope env = new RequestRegistroBoleto.Envelope()
            {
                Body = new RequestRegistroBoleto.EnvelopeBody
                {
                    requisicao = new RequestRegistroBoleto.requisicao
                    {
                        numeroConvenio = 12313,
                        valorOriginalTitulo = 0
                    }
                }
            };

            var errors = "";
            /* Este objeto possui os metos de authenticar e registrar um boleto no banco do brasil 
             * * OBS: metodos executados automaticamente após chamar RegistrarBoletoBancoBrasil */

            Operacao operacao = new Operacao(EnumTipoSistema.BancoBrasil);
            var retorno = operacao.RegistrarBoletoBancoBrasil(ClientID, ClientSecret, EnumTipoAplicacao.Homologação, env, out errors);
            
            if(errors == null)
            {

                /* retorno.Body.resposta - Dentro deste objeto possui varias propriedades */
                MessageBox.Show(retorno.Body.resposta.textoMensagemErro.ToString());
                var erroBoleto = retorno.Body.resposta.textoMensagemErro;

            }
            else{
                MessageBox.Show(errors);
            }
           
        }

        

       

       

    }

}

