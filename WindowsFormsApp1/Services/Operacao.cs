﻿using Newtonsoft.Json;
using RestSharp;
using Services.BancoBrasil;
using System;
using System.Collections.Generic;
using System.IO;
using System.Net;
using System.Text;
using System.Xml;
using static Services.BancoBrasil.RequestRegistroBoleto;

namespace Services
{
    public class Operacao
    {
        EnumTipoSistema _tipoSistema;
        RetornoRegistroBoleto.Envelope _envelope = new RetornoRegistroBoleto.Envelope();
        RetornoOath auth = new RetornoOath();
        private string linkOath = "";
        private string linkRegistroBoleto = "";
        private string KeyAuthetication = "";

        public Operacao(EnumTipoSistema tipoSistema)
        {
            _tipoSistema = tipoSistema;
        }

        public void buscarTipo()
        {
            var tipo = _tipoSistema;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="ClientID"> chave ClientID Banco do Brasil </param>
        /// <param name="ClientSecret"> Chave ClientSecret Banco do Brasil</param>
        /// <param name="tipoAplicacao"> Tipo de aplicação: Homologação ou Produção</param>
        /// <param name="envelope"> Objeto preenchido com os dados do Boleto</param>
        /// <param name="errors"> ´Caso possua erros irá retornar um valor</param>
        /// <returns></returns>
        public RetornoRegistroBoleto.Envelope RegistrarBoletoBancoBrasil(string ClientID, string ClientSecret, EnumTipoAplicacao tipoAplicacao,RequestRegistroBoleto.Envelope envelope, out string errors)
        {
            KeyAuthetication = ConvertStringToBase64(ClientID, ClientSecret);

            if (tipoAplicacao == EnumTipoAplicacao.Homologação)
            {
                linkOath = UrlTipoServidorBB.OAuthHomologacao;
                linkRegistroBoleto = UrlTipoServidorBB.registraBoletoHomologacao;
            }
            if (tipoAplicacao == EnumTipoAplicacao.Produçao)
            {
                linkOath = UrlTipoServidorBB.OAuthProducao;
                linkRegistroBoleto = UrlTipoServidorBB.registraBoletoProducao;
            }

            RetornoOath isAuthentic = AuthenticarBancoBrasil();
           
            if (isAuthentic.error == null)
            {
               errors = null;
               return EnviarXml(envelope);
            }
            else
            {
               errors = isAuthentic.error_description;
               return null;
            }

        }

        private RetornoOath AuthenticarBancoBrasil()
        {
            try
            {
                var client = new RestClient(linkOath);
                var request = new RestRequest(Method.POST);
                request.AddHeader("cache-control", "no-cache");
                request.AddHeader("content-type", "application/x-www-form-urlencoded");
                request.AddHeader("authorization", "Basic " + KeyAuthetication);
                IRestResponse response = client.Execute(request);

                var erro = response.StatusCode;
                if (response.StatusCode == HttpStatusCode.Unauthorized)
                {
                    Console.WriteLine("Erro ao Authentiacar! tente novamente.");
                }
                var result = JsonConvert.DeserializeObject<RetornoOath>(response.Content);
                auth = result;

                if(auth.error == null)
                {
                    return new RetornoOath();
                }
                else
                {
                    return new RetornoOath { error= result.error, error_description = result.error_description};
                }
            }
            catch (Exception ex)
            {
                return new RetornoOath(); ;
            }
        }
        

       


        private RetornoRegistroBoleto.Envelope EnviarXml(RequestRegistroBoleto.Envelope env)
        {
            HttpWebRequest request = CreateSOAPWebRequest();
            XmlDocument SOAPReqBody = new XmlDocument();

            var registroBoletoXmlString = Converter.ParaString<RequestRegistroBoleto.Envelope>(env);
            SOAPReqBody.LoadXml(registroBoletoXmlString);
            using (Stream stream = request.GetRequestStream())
            {
                SOAPReqBody.Save(stream);
            }

            ServicePointManager.ServerCertificateValidationCallback += (sender, certificate, chain, errors) => { return true; };
            ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls | SecurityProtocolType.Tls11 | SecurityProtocolType.Tls12 | SecurityProtocolType.Tls13;

            try
            {
                //Geting response from request  
                using (WebResponse Serviceres = request.GetResponse())
                {
                    using (StreamReader rd = new StreamReader(Serviceres.GetResponseStream()))
                    {
                        //reading stream  
                        var ServiceResult = rd.ReadToEnd();
                        RetornoRegistroBoleto.Envelope envelope = (RetornoRegistroBoleto.Envelope)Converter.ParaObjeto(ServiceResult, _envelope.GetType());
                        var codigoBarras = envelope.Body.resposta.codigoBarraNumerico.ToString();
                        return envelope;
                        //writting stream result on console  
                    }
                }
            }
            catch (Exception ex)
            {
                return null;
            }

        }


        private string ConvertStringToBase64(string clientID, string ClientSecret)
        {
            #region mostrar codigo
            var plainTextBytes = System.Text.Encoding.UTF8.GetBytes(clientID + ":" + ClientSecret);
            return System.Convert.ToBase64String(plainTextBytes);
            #endregion
        }

        public HttpWebRequest CreateSOAPWebRequest()
        {
            #region mostrar codigo
            HttpWebRequest Req = (HttpWebRequest)WebRequest.Create(linkRegistroBoleto);
            //SOAPAction    
            Req.Headers.Add(@"SOAPACTION:registrarBoleto");
            Req.Headers.Add(@"Authorization:Bearer " + auth.access_token);
            // Req.ClientCertificates.Add(new System.Security.Cryptography.X509Certificates.X509Certificate2("Certificado.pfx", "Neo.18"));
            Req.ContentType = "text/xml;charset=\"utf-8\"";
            //Req.Accept = "text/xml";
            //HTTP method    
            Req.Method = "POST";
            return Req;
            #endregion
        }

    }
}
