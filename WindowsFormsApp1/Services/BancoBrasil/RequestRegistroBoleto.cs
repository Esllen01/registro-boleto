﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Services.BancoBrasil
{
    public class RequestRegistroBoleto
    {
        // NOTE: Generated code may require at least .NET Framework 4.5 or .NET Core/Standard 2.0.
        /// <remarks/>
        [System.SerializableAttribute()]
        [System.ComponentModel.DesignerCategoryAttribute("code")]
        [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true, Namespace = "http://schemas.xmlsoap.org/soap/envelope/")]
        [System.Xml.Serialization.XmlRootAttribute(Namespace = "http://schemas.xmlsoap.org/soap/envelope/", IsNullable = false)]
        public partial class Envelope
        {

            private object headerField;

            private EnvelopeBody bodyField;

            /// <remarks/>
            public object Header
            {
                get
                {
                    return this.headerField;
                }
                set
                {
                    this.headerField = value;
                }
            }

            /// <remarks/>
            public EnvelopeBody Body
            {
                get
                {
                    return this.bodyField;
                }
                set
                {
                    this.bodyField = value;
                }
            }
        }

        /// <remarks/>
        [System.SerializableAttribute()]
        [System.ComponentModel.DesignerCategoryAttribute("code")]
        [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true, Namespace = "http://schemas.xmlsoap.org/soap/envelope/")]
        public partial class EnvelopeBody
        {

            private requisicao requisicaoField;

            /// <remarks/>
            [System.Xml.Serialization.XmlElementAttribute(Namespace = "http://www.tibco.com/schemas/bws_registro_cbr/Recursos/XSD/Schema.xsd")]
            public requisicao requisicao
            {
                get
                {
                    return this.requisicaoField;
                }
                set
                {
                    this.requisicaoField = value;
                }
            }
        }

        /// <remarks/>
        [System.SerializableAttribute()]
        [System.ComponentModel.DesignerCategoryAttribute("code")]
        [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true, Namespace = "http://www.tibco.com/schemas/bws_registro_cbr/Recursos/XSD/Schema.xsd")]
        [System.Xml.Serialization.XmlRootAttribute(Namespace = "http://www.tibco.com/schemas/bws_registro_cbr/Recursos/XSD/Schema.xsd", IsNullable = false)]
        public partial class requisicao
        {

            private uint numeroConvenioField;

            private byte numeroCarteiraField;

            private byte numeroVariacaoCarteiraField;

            private byte codigoModalidadeTituloField;

            private string dataEmissaoTituloField;

            private string dataVencimentoTituloField;

            private ushort valorOriginalTituloField;

            private byte codigoTipoDescontoField;

            private string dataDescontoTituloField;

            private object percentualDescontoTituloField;

            private byte valorDescontoTituloField;

            private object valorAbatimentoTituloField;

            private byte quantidadeDiaProtestoField;

            private byte codigoTipoJuroMoraField;

            private object percentualJuroMoraTituloField;

            private object valorJuroMoraTituloField;

            private byte codigoTipoMultaField;

            private string dataMultaTituloField;

            private byte percentualMultaTituloField;

            private object valorMultaTituloField;

            private string codigoAceiteTituloField;

            private byte codigoTipoTituloField;

            private string textoDescricaoTipoTituloField;

            private string indicadorPermissaoRecebimentoParcialField;

            private ulong textoNumeroTituloBeneficiarioField;

            private object textoCampoUtilizacaoBeneficiarioField;

            private byte codigoTipoContaCaucaoField;

            private ulong textoNumeroTituloClienteField;

            private string textoMensagemBloquetoOcorrenciaField;

            private byte codigoTipoInscricaoPagadorField;

            private ulong numeroInscricaoPagadorField;

            private string nomePagadorField;

            private string textoEnderecoPagadorField;

            private uint numeroCepPagadorField;

            private string nomeMunicipioPagadorField;

            private string nomeBairroPagadorField;

            private string siglaUfPagadorField;

            private uint textoNumeroTelefonePagadorField;

            private object codigoTipoInscricaoAvalistaField;

            private object numeroInscricaoAvalistaField;

            private object nomeAvalistaTituloField;

            private byte codigoChaveUsuarioField;

            private byte codigoTipoCanalSolicitacaoField;

            /// <remarks/>
            public uint numeroConvenio
            {
                get
                {
                    return this.numeroConvenioField;
                }
                set
                {
                    this.numeroConvenioField = value;
                }
            }

            /// <remarks/>
            public byte numeroCarteira
            {
                get
                {
                    return this.numeroCarteiraField;
                }
                set
                {
                    this.numeroCarteiraField = value;
                }
            }

            /// <remarks/>
            public byte numeroVariacaoCarteira
            {
                get
                {
                    return this.numeroVariacaoCarteiraField;
                }
                set
                {
                    this.numeroVariacaoCarteiraField = value;
                }
            }

            /// <remarks/>
            public byte codigoModalidadeTitulo
            {
                get
                {
                    return this.codigoModalidadeTituloField;
                }
                set
                {
                    this.codigoModalidadeTituloField = value;
                }
            }

            /// <remarks/>
            public string dataEmissaoTitulo
            {
                get
                {
                    return this.dataEmissaoTituloField;
                }
                set
                {
                    this.dataEmissaoTituloField = value;
                }
            }

            /// <remarks/>
            public string dataVencimentoTitulo
            {
                get
                {
                    return this.dataVencimentoTituloField;
                }
                set
                {
                    this.dataVencimentoTituloField = value;
                }
            }

            /// <remarks/>
            public ushort valorOriginalTitulo
            {
                get
                {
                    return this.valorOriginalTituloField;
                }
                set
                {
                    this.valorOriginalTituloField = value;
                }
            }

            /// <remarks/>
            public byte codigoTipoDesconto
            {
                get
                {
                    return this.codigoTipoDescontoField;
                }
                set
                {
                    this.codigoTipoDescontoField = value;
                }
            }

            /// <remarks/>
            public string dataDescontoTitulo
            {
                get
                {
                    return this.dataDescontoTituloField;
                }
                set
                {
                    this.dataDescontoTituloField = value;
                }
            }

            /// <remarks/>
            public object percentualDescontoTitulo
            {
                get
                {
                    return this.percentualDescontoTituloField;
                }
                set
                {
                    this.percentualDescontoTituloField = value;
                }
            }

            /// <remarks/>
            public byte valorDescontoTitulo
            {
                get
                {
                    return this.valorDescontoTituloField;
                }
                set
                {
                    this.valorDescontoTituloField = value;
                }
            }

            /// <remarks/>
            public object valorAbatimentoTitulo
            {
                get
                {
                    return this.valorAbatimentoTituloField;
                }
                set
                {
                    this.valorAbatimentoTituloField = value;
                }
            }

            /// <remarks/>
            public byte quantidadeDiaProtesto
            {
                get
                {
                    return this.quantidadeDiaProtestoField;
                }
                set
                {
                    this.quantidadeDiaProtestoField = value;
                }
            }

            /// <remarks/>
            public byte codigoTipoJuroMora
            {
                get
                {
                    return this.codigoTipoJuroMoraField;
                }
                set
                {
                    this.codigoTipoJuroMoraField = value;
                }
            }

            /// <remarks/>
            public object percentualJuroMoraTitulo
            {
                get
                {
                    return this.percentualJuroMoraTituloField;
                }
                set
                {
                    this.percentualJuroMoraTituloField = value;
                }
            }

            /// <remarks/>
            public object valorJuroMoraTitulo
            {
                get
                {
                    return this.valorJuroMoraTituloField;
                }
                set
                {
                    this.valorJuroMoraTituloField = value;
                }
            }

            /// <remarks/>
            public byte codigoTipoMulta
            {
                get
                {
                    return this.codigoTipoMultaField;
                }
                set
                {
                    this.codigoTipoMultaField = value;
                }
            }

            /// <remarks/>
            public string dataMultaTitulo
            {
                get
                {
                    return this.dataMultaTituloField;
                }
                set
                {
                    this.dataMultaTituloField = value;
                }
            }

            /// <remarks/>
            public byte percentualMultaTitulo
            {
                get
                {
                    return this.percentualMultaTituloField;
                }
                set
                {
                    this.percentualMultaTituloField = value;
                }
            }

            /// <remarks/>
            public object valorMultaTitulo
            {
                get
                {
                    return this.valorMultaTituloField;
                }
                set
                {
                    this.valorMultaTituloField = value;
                }
            }

            /// <remarks/>
            public string codigoAceiteTitulo
            {
                get
                {
                    return this.codigoAceiteTituloField;
                }
                set
                {
                    this.codigoAceiteTituloField = value;
                }
            }

            /// <remarks/>
            public byte codigoTipoTitulo
            {
                get
                {
                    return this.codigoTipoTituloField;
                }
                set
                {
                    this.codigoTipoTituloField = value;
                }
            }

            /// <remarks/>
            public string textoDescricaoTipoTitulo
            {
                get
                {
                    return this.textoDescricaoTipoTituloField;
                }
                set
                {
                    this.textoDescricaoTipoTituloField = value;
                }
            }

            /// <remarks/>
            public string indicadorPermissaoRecebimentoParcial
            {
                get
                {
                    return this.indicadorPermissaoRecebimentoParcialField;
                }
                set
                {
                    this.indicadorPermissaoRecebimentoParcialField = value;
                }
            }

            /// <remarks/>
            public ulong textoNumeroTituloBeneficiario
            {
                get
                {
                    return this.textoNumeroTituloBeneficiarioField;
                }
                set
                {
                    this.textoNumeroTituloBeneficiarioField = value;
                }
            }

            /// <remarks/>
            public object textoCampoUtilizacaoBeneficiario
            {
                get
                {
                    return this.textoCampoUtilizacaoBeneficiarioField;
                }
                set
                {
                    this.textoCampoUtilizacaoBeneficiarioField = value;
                }
            }

            /// <remarks/>
            public byte codigoTipoContaCaucao
            {
                get
                {
                    return this.codigoTipoContaCaucaoField;
                }
                set
                {
                    this.codigoTipoContaCaucaoField = value;
                }
            }

            /// <remarks/>
            public ulong textoNumeroTituloCliente
            {
                get
                {
                    return this.textoNumeroTituloClienteField;
                }
                set
                {
                    this.textoNumeroTituloClienteField = value;
                }
            }

            /// <remarks/>
            public string textoMensagemBloquetoOcorrencia
            {
                get
                {
                    return this.textoMensagemBloquetoOcorrenciaField;
                }
                set
                {
                    this.textoMensagemBloquetoOcorrenciaField = value;
                }
            }

            /// <remarks/>
            public byte codigoTipoInscricaoPagador
            {
                get
                {
                    return this.codigoTipoInscricaoPagadorField;
                }
                set
                {
                    this.codigoTipoInscricaoPagadorField = value;
                }
            }

            /// <remarks/>
            public ulong numeroInscricaoPagador
            {
                get
                {
                    return this.numeroInscricaoPagadorField;
                }
                set
                {
                    this.numeroInscricaoPagadorField = value;
                }
            }

            /// <remarks/>
            public string nomePagador
            {
                get
                {
                    return this.nomePagadorField;
                }
                set
                {
                    this.nomePagadorField = value;
                }
            }

            /// <remarks/>
            public string textoEnderecoPagador
            {
                get
                {
                    return this.textoEnderecoPagadorField;
                }
                set
                {
                    this.textoEnderecoPagadorField = value;
                }
            }

            /// <remarks/>
            public uint numeroCepPagador
            {
                get
                {
                    return this.numeroCepPagadorField;
                }
                set
                {
                    this.numeroCepPagadorField = value;
                }
            }

            /// <remarks/>
            public string nomeMunicipioPagador
            {
                get
                {
                    return this.nomeMunicipioPagadorField;
                }
                set
                {
                    this.nomeMunicipioPagadorField = value;
                }
            }

            /// <remarks/>
            public string nomeBairroPagador
            {
                get
                {
                    return this.nomeBairroPagadorField;
                }
                set
                {
                    this.nomeBairroPagadorField = value;
                }
            }

            /// <remarks/>
            public string siglaUfPagador
            {
                get
                {
                    return this.siglaUfPagadorField;
                }
                set
                {
                    this.siglaUfPagadorField = value;
                }
            }

            /// <remarks/>
            public uint textoNumeroTelefonePagador
            {
                get
                {
                    return this.textoNumeroTelefonePagadorField;
                }
                set
                {
                    this.textoNumeroTelefonePagadorField = value;
                }
            }

            /// <remarks/>
            public object codigoTipoInscricaoAvalista
            {
                get
                {
                    return this.codigoTipoInscricaoAvalistaField;
                }
                set
                {
                    this.codigoTipoInscricaoAvalistaField = value;
                }
            }

            /// <remarks/>
            public object numeroInscricaoAvalista
            {
                get
                {
                    return this.numeroInscricaoAvalistaField;
                }
                set
                {
                    this.numeroInscricaoAvalistaField = value;
                }
            }

            /// <remarks/>
            public object nomeAvalistaTitulo
            {
                get
                {
                    return this.nomeAvalistaTituloField;
                }
                set
                {
                    this.nomeAvalistaTituloField = value;
                }
            }

            /// <remarks/>
            public byte codigoChaveUsuario
            {
                get
                {
                    return this.codigoChaveUsuarioField;
                }
                set
                {
                    this.codigoChaveUsuarioField = value;
                }
            }

            /// <remarks/>
            public byte codigoTipoCanalSolicitacao
            {
                get
                {
                    return this.codigoTipoCanalSolicitacaoField;
                }
                set
                {
                    this.codigoTipoCanalSolicitacaoField = value;
                }
            }
        }
    }
}
