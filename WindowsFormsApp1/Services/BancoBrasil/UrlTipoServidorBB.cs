﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Services.BancoBrasil
{
    public static class UrlTipoServidorBB
    {
        public static string OAuthHomologacao = "https://oauth.hm.bb.com.br/oauth/token?grant_type=client_credentials&scope=cobranca.registro-boletos";
        public static string OAuthProducao = "https://oauth.bb.com.br/oauth/token?grant_type=client_credentials&scope=cobranca.registro-boletos";
        
        public static string registraBoletoHomologacao = "https://cobranca.homologa.bb.com.br:7101/registrarBoleto";
        public static string registraBoletoProducao = "https://cobranca.bb.com.br:7101/registrarBoleto";

    }   
}
