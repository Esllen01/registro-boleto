﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Services.BancoBrasil
{
    public class RetornoRegistroBoleto
    {
        // NOTE: Generated code may require at least .NET Framework 4.5 or .NET Core/Standard 2.0.
        /// <remarks/>
        [System.SerializableAttribute()]
        [System.ComponentModel.DesignerCategoryAttribute("code")]
        [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true, Namespace = "http://schemas.xmlsoap.org/soap/envelope/")]
        [System.Xml.Serialization.XmlRootAttribute(Namespace = "http://schemas.xmlsoap.org/soap/envelope/", IsNullable = false)]
        public partial class Envelope
        {

            private EnvelopeBody bodyField;

            /// <remarks/>
            public EnvelopeBody Body
            {
                get
                {
                    return this.bodyField;
                }
                set
                {
                    this.bodyField = value;
                }
            }
        }

        /// <remarks/>
        [System.SerializableAttribute()]
        [System.ComponentModel.DesignerCategoryAttribute("code")]
        [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true, Namespace = "http://schemas.xmlsoap.org/soap/envelope/")]
        public partial class EnvelopeBody
        {

            private resposta respostaField;

            /// <remarks/>
            [System.Xml.Serialization.XmlElementAttribute(Namespace = "http://www.tibco.com/schemas/bws_registro_cbr/Recursos/XSD/Schema.xsd")]
            public resposta resposta
            {
                get
                {
                    return this.respostaField;
                }
                set
                {
                    this.respostaField = value;
                }
            }
        }

        /// <remarks/>
        [System.SerializableAttribute()]
        [System.ComponentModel.DesignerCategoryAttribute("code")]
        [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true, Namespace = "http://www.tibco.com/schemas/bws_registro_cbr/Recursos/XSD/Schema.xsd")]
        [System.Xml.Serialization.XmlRootAttribute(Namespace = "http://www.tibco.com/schemas/bws_registro_cbr/Recursos/XSD/Schema.xsd", IsNullable = false)]
        public partial class resposta
        {

            private object siglaSistemaMensagemField;

            private byte codigoRetornoProgramaField;

            private string nomeProgramaErroField;

            private string textoMensagemErroField;

            private byte numeroPosicaoErroProgramaField;

            private byte codigoTipoRetornoProgramaField;

            private object textoNumeroTituloCobrancaBbField;

            private byte numeroCarteiraCobrancaField;

            private byte numeroVariacaoCarteiraCobrancaField;

            private ushort codigoPrefixoDependenciaBeneficiarioField;

            private ushort numeroContaCorrenteBeneficiarioField;

            private uint codigoClienteField;

            private object linhaDigitavelField;

            private object codigoBarraNumericoField;

            private byte codigoTipoEnderecoBeneficiarioField;

            private object nomeLogradouroBeneficiarioField;

            private object nomeBairroBeneficiarioField;

            private object nomeMunicipioBeneficiarioField;

            private byte codigoMunicipioBeneficiarioField;

            private object siglaUfBeneficiarioField;

            private byte codigoCepBeneficiarioField;

            private object indicadorComprovacaoBeneficiarioField;

            private uint numeroContratoCobrancaField;

            /// <remarks/>
            public object siglaSistemaMensagem
            {
                get
                {
                    return this.siglaSistemaMensagemField;
                }
                set
                {
                    this.siglaSistemaMensagemField = value;
                }
            }

            /// <remarks/>
            public byte codigoRetornoPrograma
            {
                get
                {
                    return this.codigoRetornoProgramaField;
                }
                set
                {
                    this.codigoRetornoProgramaField = value;
                }
            }

            /// <remarks/>
            public string nomeProgramaErro
            {
                get
                {
                    return this.nomeProgramaErroField;
                }
                set
                {
                    this.nomeProgramaErroField = value;
                }
            }

            /// <remarks/>
            public string textoMensagemErro
            {
                get
                {
                    return this.textoMensagemErroField;
                }
                set
                {
                    this.textoMensagemErroField = value;
                }
            }

            /// <remarks/>
            public byte numeroPosicaoErroPrograma
            {
                get
                {
                    return this.numeroPosicaoErroProgramaField;
                }
                set
                {
                    this.numeroPosicaoErroProgramaField = value;
                }
            }

            /// <remarks/>
            public byte codigoTipoRetornoPrograma
            {
                get
                {
                    return this.codigoTipoRetornoProgramaField;
                }
                set
                {
                    this.codigoTipoRetornoProgramaField = value;
                }
            }

            /// <remarks/>
            public object textoNumeroTituloCobrancaBb
            {
                get
                {
                    return this.textoNumeroTituloCobrancaBbField;
                }
                set
                {
                    this.textoNumeroTituloCobrancaBbField = value;
                }
            }

            /// <remarks/>
            public byte numeroCarteiraCobranca
            {
                get
                {
                    return this.numeroCarteiraCobrancaField;
                }
                set
                {
                    this.numeroCarteiraCobrancaField = value;
                }
            }

            /// <remarks/>
            public byte numeroVariacaoCarteiraCobranca
            {
                get
                {
                    return this.numeroVariacaoCarteiraCobrancaField;
                }
                set
                {
                    this.numeroVariacaoCarteiraCobrancaField = value;
                }
            }

            /// <remarks/>
            public ushort codigoPrefixoDependenciaBeneficiario
            {
                get
                {
                    return this.codigoPrefixoDependenciaBeneficiarioField;
                }
                set
                {
                    this.codigoPrefixoDependenciaBeneficiarioField = value;
                }
            }

            /// <remarks/>
            public ushort numeroContaCorrenteBeneficiario
            {
                get
                {
                    return this.numeroContaCorrenteBeneficiarioField;
                }
                set
                {
                    this.numeroContaCorrenteBeneficiarioField = value;
                }
            }

            /// <remarks/>
            public uint codigoCliente
            {
                get
                {
                    return this.codigoClienteField;
                }
                set
                {
                    this.codigoClienteField = value;
                }
            }

            /// <remarks/>
            public object linhaDigitavel
            {
                get
                {
                    return this.linhaDigitavelField;
                }
                set
                {
                    this.linhaDigitavelField = value;
                }
            }

            /// <remarks/>
            public object codigoBarraNumerico
            {
                get
                {
                    return this.codigoBarraNumericoField;
                }
                set
                {
                    this.codigoBarraNumericoField = value;
                }
            }

            /// <remarks/>
            public byte codigoTipoEnderecoBeneficiario
            {
                get
                {
                    return this.codigoTipoEnderecoBeneficiarioField;
                }
                set
                {
                    this.codigoTipoEnderecoBeneficiarioField = value;
                }
            }

            /// <remarks/>
            public object nomeLogradouroBeneficiario
            {
                get
                {
                    return this.nomeLogradouroBeneficiarioField;
                }
                set
                {
                    this.nomeLogradouroBeneficiarioField = value;
                }
            }

            /// <remarks/>
            public object nomeBairroBeneficiario
            {
                get
                {
                    return this.nomeBairroBeneficiarioField;
                }
                set
                {
                    this.nomeBairroBeneficiarioField = value;
                }
            }

            /// <remarks/>
            public object nomeMunicipioBeneficiario
            {
                get
                {
                    return this.nomeMunicipioBeneficiarioField;
                }
                set
                {
                    this.nomeMunicipioBeneficiarioField = value;
                }
            }

            /// <remarks/>
            public byte codigoMunicipioBeneficiario
            {
                get
                {
                    return this.codigoMunicipioBeneficiarioField;
                }
                set
                {
                    this.codigoMunicipioBeneficiarioField = value;
                }
            }

            /// <remarks/>
            public object siglaUfBeneficiario
            {
                get
                {
                    return this.siglaUfBeneficiarioField;
                }
                set
                {
                    this.siglaUfBeneficiarioField = value;
                }
            }

            /// <remarks/>
            public byte codigoCepBeneficiario
            {
                get
                {
                    return this.codigoCepBeneficiarioField;
                }
                set
                {
                    this.codigoCepBeneficiarioField = value;
                }
            }

            /// <remarks/>
            public object indicadorComprovacaoBeneficiario
            {
                get
                {
                    return this.indicadorComprovacaoBeneficiarioField;
                }
                set
                {
                    this.indicadorComprovacaoBeneficiarioField = value;
                }
            }

            /// <remarks/>
            public uint numeroContratoCobranca
            {
                get
                {
                    return this.numeroContratoCobrancaField;
                }
                set
                {
                    this.numeroContratoCobrancaField = value;
                }
            }
        }
    }
}
