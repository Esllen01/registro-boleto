﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Text;
using System.Xml.Serialization;

namespace Services
{
    public static class Converter
    {
        public static object ParaObjeto(string valor, Type tipo)
        {
            try
            {
                XmlSerializer xml = new XmlSerializer(tipo);
                var valor_serealizado = new StringReader(valor);
                return xml.Deserialize(valor_serealizado);
            }
            catch
            {
                throw;
            }
        }

        public static string ParaString<T>(this T valor)
        {
            try
            {
                XmlSerializer xml = new XmlSerializer(valor.GetType());
                StringWriter retorno = new StringWriter();
                xml.Serialize(retorno, valor);
                var replace1 = retorno.ToString().Replace("\r\n", "");
                var replace2 = replace1.Replace("  ", "");
                var replace3 = replace2.Replace("utf-16", "utf-8");
                return replace3;
            }
            catch
            {
                throw;
            }
        }
    }
}
